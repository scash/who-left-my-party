Who Left My Party?
==================
An Android application that will enable users to track who left their social network party. In other words, a user will be able to see who no longer follows them on Twitter, Instagram, LinkedIn, Facebook and any other major social network that allows access to their API.

--------------------
## Notes
1. This application relies on the [Twitter4j 3.0.5](http://twitter4j.org/en/index.html) library, which you can conveniently find in the `libs` directory. Should the Twitter API change, you may need to update to a more recent version of the library.

2. Update the `TwitterValues` class and change the values for `TWITTER_CONSUMER_KEY` and `TWITTER_CONSUMER_SECRET` to the values listed on the Twitter developer console for your app.

3. At the moment the only online social network supported is Twitter.

4. This is a pre-alpha release, so there is a lot of code clean-up that has yet to take place.

5. There are some major UI limitations, but remeber, this is a pre-alpha release. That and many other enhancements are forthcoming.

## Credits

  - As stated above, this application leverages the [Twitter4j 3.0.5](http://twitter4j.org/en/index.html) library.
  - Capitrium's [TwitterAuth](https://github.com/Capitrium/TwitterAuth) was used as the baseline for Oauth authentication with Twitter.
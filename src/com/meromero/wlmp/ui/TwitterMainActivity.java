package com.meromero.wlmp.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.meromero.wlmp.R;
import com.meromero.wlmp.twitter.TwitterTasks;
import com.meromero.wlmp.twitter.TwitterValues;

/**
 * Twitter main activity
 *
 * <p>
 * This activity loads and binds UI views for adding
 * as well as displaying existing Twitter account(s).
 * </p>
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class TwitterMainActivity extends Activity implements OnItemClickListener {

	private static final String LOGCAT = TwitterMainActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_main);

		processTwitterLogin();
	}

	@Override
	public void onResume() {
		super.onResume();

		displayAccts();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(this, TwitterDetailActivity.class);
		intent.putExtra("tid", id);
		startActivity(intent);
	}

	/**
	 * Initiate Twitter authentication process
	 */
	public void clickTwitterLogin(View v) {
//		if(!TwitterValues.TWITTER_IS_LOGGED_IN) {
//			new TwitterTasks.TwitterGetRequestTokenTask(this).execute();				
//		} else {
//			TwitterTasks.logoutFromTwitter();
//		}

		new TwitterTasks.TwitterGetRequestTokenTask(this).execute();
	}

	/**
	 * This method handles any incoming Twitter authentication
	 * 
	 * <p>
	 * Upon activity creation, this method will check to see if the user is coming
	 * from the Twitter authentication page. If so, it will authenticate and store
	 * token information in persistent storage (database).
	 * </p>
	 */
	public void processTwitterLogin() {
		Uri uri = getIntent().getData();

//		if(uri != null) {
//			Log.i(LOGCAT, "returned url from twitter: " + uri.toString());
//		}

		if(uri != null && uri.toString().startsWith(TwitterValues.TWITTER_CALLBACK_URL)) {
			String verifier = uri.getQueryParameter(TwitterValues.URL_PARAMETER_TWITTER_OAUTH_VERIFIER);

			new TwitterTasks.TwitterGetAccessTokenTask(this, this).execute(verifier);

			Log.i(LOGCAT, "uri contains oauth_verifier");
		}
	}

	/**
	 * Retrieve existing TwitterAccounts from database and display
	 */
	public void displayAccts() {
		new TwitterTasks.DisplayAccts(this, (ListView) findViewById(R.id.lv_accts), R.layout.item_twitter_accts).execute();
	}

}
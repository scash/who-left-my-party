package com.meromero.wlmp.ui;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.meromero.wlmp.R;
import com.meromero.wlmp.model.TwitterAccount;
import com.meromero.wlmp.twitter.ImageHelper;
import com.meromero.wlmp.twitter.TwitterTasks;

/**
 * Twitter detail activity
 *
 * <p>
 * This activity processes and displays detailed
 * information related to the selected Twitter account.
 * </p>
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class TwitterDetailActivity extends Activity {
	//
	// TODO: Show recent unfollowers
	//

	private long tid;
	private TwitterAccount ta;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_detail);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		// Retrieve passed id
		Intent intent = getIntent();
		tid = intent.getLongExtra("tid", 0);

		if (tid != 0) {
			// Retrieve TwitterAccount associated with the passed id			
			new TwitterTasks.GetAcct(this).execute(tid);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.twitter_detail, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * This method is invoked from the worker thread to retrieve
	 * the {@link TwitterAccount} object associated with the
	 * specified Twitter account
	 *
	 * @param	ta	a {@link TwitterAccount} object representing the specified Twitter account
	 */
	public void processWorkerThreadResult(TwitterAccount ta) {
		this.ta = ta;	

		// Set the activity label to the Twitter handle
		setTitle(ta.getHandle());

		// Populate layout views
		processLayout();

		// Handle any necessary database updates
		processDbUpdates();
	}

	/**
	 * Logout the Twitter account from the application
	 *
	 * @param item	the {@link MenuItem} object
	 */
	public void logOut(MenuItem item) {

		// Start worker thread to delete Twitter account from the database
		new TwitterTasks.LogOut(this).execute(tid);

		finish();
	}

	/**
	 * Bind data to layout views
	 */
	private void processLayout() {
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");

		// Handle Twitter avatar
		ImageView iv = (ImageView) findViewById(R.id.twitter_avatar);
		new ImageHelper.CreateBitmapFromUrl(iv).execute(ta.getTwitterUser().getBiggerProfileImageURL());

		TextView tvHandle = (TextView) findViewById(R.id.twitter_handle);
		tvHandle.setText(ta.getHandle());

		TextView tvFollowers = (TextView) findViewById(R.id.twitter_followers);
		tvFollowers.setText("Followers: " + Integer.toString(ta.getFollowers()));

		TextView tvFollowing = (TextView) findViewById(R.id.twitter_following);
		tvFollowing.setText("Following: " + Integer.toString(ta.getFollowing()));

		TextView tvSince = (TextView) findViewById(R.id.twitter_since);
		tvSince.setText("Joined: " + sdf.format(ta.getTwitterUser().getCreatedAt()));
	}

	/**
	 * Process any necessary database transactions
	 */
	private void processDbUpdates() {
		// Store followers in database
		new TwitterTasks.StoreFollowersIds(this).execute(ta);
	}

}
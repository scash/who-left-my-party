package com.meromero.wlmp.twitter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meromero.wlmp.R;
import com.meromero.wlmp.model.TwitterAccount;

/**
 * An adapter used to bind data to the layout that displays
 * all authenticated Twitter accounts stored by the application
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class DisplayAccountsAdapter extends BaseAdapter {

//	private Context context;
	private LayoutInflater li;
	private int layoutResourceId;
	private List<TwitterAccount> accts;

	DisplayAccountsAdapter(Context context, int layoutResourceId, List<TwitterAccount> accts) {
//		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.accts = accts;

		li = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parentView) {
		ViewHolder hv;
		TwitterAccount ta = (TwitterAccount) getItem(position);

		if (convertView == null) {
//			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = li.inflate(layoutResourceId, parentView, false);

			hv = new ViewHolder();

			// Obtain all views and store in the ViewHolder
			hv.iv = (ImageView) convertView.findViewById(R.id.twitter_avatar);
			hv.tvHandle = (TextView) convertView.findViewById(R.id.twitter_handle);
			hv.tvFollowers = (TextView) convertView.findViewById(R.id.twitter_followers);
			hv.tvFollowing = (TextView) convertView.findViewById(R.id.twitter_following);

			convertView.setTag(hv);
		} else {
	        hv = (ViewHolder) convertView.getTag();
		}

		// Handle Twitter avatar
		new ImageHelper.CreateBitmapFromUrl(hv.iv).execute(ta.getTwitterUser().getProfileImageURL());

		hv.tvHandle.setText(ta.getHandle());
		hv.tvFollowers.setText("Followers: " + Integer.toString(ta.getFollowers()));
		hv.tvFollowing.setText("Following: " + Integer.toString(ta.getFollowing()));

		return convertView;
	}

	@Override
	public int getCount() {
		return accts.size();
	}

	@Override
	public Object getItem(int position) {
		return accts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return accts.get(position).getTid();
	}

    /**
     * Static class used to enhance performance through the application of the View Holder design pattern
     */
	private static class ViewHolder {
		private ImageView iv;
		private TextView tvHandle, tvFollowers, tvFollowing;
	}

}
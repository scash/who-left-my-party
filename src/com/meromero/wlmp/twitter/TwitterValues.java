package com.meromero.wlmp.twitter;

import twitter4j.Twitter;
import twitter4j.User;

/**
* Holds the consumer key/secret used to connect with an app registered
* on Twitter, as well as a callback url containing the scheme and host
* values that must be registered on the Activity that is launched when
* Twitter sends back the response to the authentication request.
*/
public class TwitterValues {
	public static String TWITTER_CONSUMER_KEY = "YOUR_CONSUMER_KEY";
	public static String TWITTER_CONSUMER_SECRET = "YOUR_CONSUMER_SECRET";
	public static String TWITTER_CALLBACK_URL = "oauth://twitter.login";
	public static String URL_PARAMETER_TWITTER_OAUTH_VERIFIER = "oauth_verifier";

	public static String TWITTER_OAUTH_TOKEN = "";
	public static String TWITTER_OAUTH_TOKEN_SECRET = "";
	public static boolean TWITTER_IS_LOGGED_IN = false;
	public static boolean TWITTER_HAS_AUTHORIZED_APP = false;

	public static User TWITTER_USER = null;
	public static Twitter TWITTER_OAUTH = null;
}
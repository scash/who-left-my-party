package com.meromero.wlmp.twitter;

import java.lang.ref.WeakReference;
import java.util.List;

import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.meromero.wlmp.db.TwitterDataSource;
import com.meromero.wlmp.model.TwitterAccount;
import com.meromero.wlmp.ui.TwitterDetailActivity;

/**
 * A class to handle various Twitter tasks
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class TwitterTasks {

	private static final String LOGCAT = TwitterTasks.class.getName();

	/**
	 * AsyncTask used to get a {@link RequestToken}, starting the Twitter login process
	 *
	 * <p>
	 * In order to launch the Twitter login page on the device's browser, the
	 * Activity calling this method must have the following in the
	 * intent-filter section of its manifest declaration:
	 *
	 * <pre>
	 * <code>
	 * &lt;category android:name="android.intent.category.BROWSABLE" /&gt;
	 * </code>
	 * </pre>
	 * </p>
	 */
	public static class TwitterGetRequestTokenTask extends AsyncTask<Void, Void, RequestToken> {

		private Activity caller;

		public TwitterGetRequestTokenTask(Activity caller) {
			super();
			this.caller = caller;
		}

		@Override
		protected RequestToken doInBackground(Void... params) {
			return TwitterUtil.getInstance().getRequestToken();
		}

		@Override
		protected void onPostExecute(RequestToken requestToken) {
			Intent intent;

			if(TwitterValues.TWITTER_HAS_AUTHORIZED_APP) {
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL()));				
			} else {
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthorizationURL()));	
			}

			caller.startActivity(intent);
			caller.finish();
		}
	}

	/**
	 * AsyncTask used to get an {@link AccessToken} to access the user's Twitter data
	 *
	 * <p>
	 * Set up an activity to start after the user logs in on Twitter's page
	 * by adding the following line to its declaration in the manifest:
	 * <pre>
	 * <code>
	 * &lt;data android:scheme="oauth" android:host="twitter.login" /&gt;
	 * </code>
	 * </pre>
	 *
	 * Then call this method from the {@link android.app.Activity#onCreate()}
	 * method using the following:
	 * <pre>
	 * <code>
	 * Uri uri = getIntent().getData();
	 *
	 * if(uri != null) {
	 * 	String verifier = uri.getQueryParameter(TwitterValues.URL_PARAMETER_TWITTER_OAUTH_VERIFIER);
	 * 	new TwitterTasks.TwitterGetAccessTokenTask(YourActivity.this).execute();
	 * }
	 * </code>
	 * </pre>
	 * </p>
	 */
	public static class TwitterGetAccessTokenTask extends AsyncTask<String, Void, Void> {

		private Activity caller, successfulLoginActivity;

		public TwitterGetAccessTokenTask(Activity caller, Activity success) {
			super();
			this.caller = caller;
			this.successfulLoginActivity = success;
		}

		@Override
		protected Void doInBackground(String... params) {
			String verifier = params[0];
			Log.i("TwitterTasks", "verifier passed was: " + verifier);

			Twitter twitter = TwitterUtil.getInstance().getTwitter();
			RequestToken requestToken = TwitterUtil.getInstance().getRequestToken();

			try {
				AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
				TwitterValues.TWITTER_OAUTH_TOKEN = accessToken.getToken();
				TwitterValues.TWITTER_OAUTH_TOKEN_SECRET = accessToken.getTokenSecret();
				TwitterValues.TWITTER_IS_LOGGED_IN = true;
				TwitterValues.TWITTER_HAS_AUTHORIZED_APP = true;
				TwitterValues.TWITTER_USER = twitter.showUser(accessToken.getUserId());

				// Ensure not a cached instance...critical if supporting multiple user Twitter accounts
				twitter.setOAuthAccessToken(accessToken);
				TwitterValues.TWITTER_OAUTH = twitter;

				// Insert TwitterAccount into database
				TwitterDataSource tds = new TwitterDataSource(caller);
				tds.open();
				tds.insertAcct(accessToken);
				tds.close();
			} catch (TwitterException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			Intent intent = null;

			if(TwitterValues.TWITTER_IS_LOGGED_IN) {
				intent = new Intent(caller, successfulLoginActivity.getClass());
				caller.startActivity(intent);
				caller.finish();
			} else {
				// Not certain why this needs to be explicit, as onPostExecute runs
				// on the UI thread...investigate (low priority)!
				caller.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(caller,
							"Failed to login via Twitter; please check"
								+ " your internet connection and try again.",
							Toast.LENGTH_LONG)
						.show();
					}
				});
			}
		}
	}

	/**
	 * An AsyncTask which will use the access token/secret for the current account
	 * to post a status to their Twitter page.
	 */
	public static class TwitterPostStatusTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... args) {
			Log.d("Tweet Text", "> " + args[0]);
			String status = args[0];

			try {
				//
				// TODO: Modify this code to leverage,
				//	Twitter twitter = TwitterUtil.getInstance().getTwitter();
				//

				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(TwitterValues.TWITTER_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TwitterValues.TWITTER_CONSUMER_SECRET);

				AccessToken accessToken = new AccessToken(
					TwitterValues.TWITTER_OAUTH_TOKEN,
					TwitterValues.TWITTER_OAUTH_TOKEN_SECRET
				);

				Twitter twitter = new TwitterFactory(builder.build()).getInstance();
				twitter.setOAuthAccessToken(accessToken);

				Log.i(LOGCAT, "Attempting to post tweet with message: " + status);

				if(status != null && status.length() > 0) {
					twitter4j.Status response = twitter.updateStatus(status);
					Log.i(LOGCAT, response.getText());
				} else {
					Log.e(LOGCAT, "Unable to submit an empty message!");
				}
			} catch (TwitterException e) {
				if(e.getErrorCode() == 187) {
					Log.i(LOGCAT, "This message has already been posted to Twitter!");
				}

				e.printStackTrace();
			}

			return null;
		}
	}

	/**
	 * Retrieve {@link TwitterAccount} object of the specified Twitter account
	 */
	public static class GetAcct extends AsyncTask<Long, Void, TwitterAccount> {

		private Twitter twitter;
		private Context context;

		public GetAcct(Context context) {
			super();
			this.context = context;

			// Obtain twitter factory instance, to pull user information
			twitter = TwitterUtil.getInstance().getTwitter();
		}

		@Override
		protected TwitterAccount doInBackground(Long... params) {
			User twitterUser;
			TwitterAccount ta;

			// Retrieve TwitterAccount from database
			TwitterDataSource tds = new TwitterDataSource(context);
			tds.open();
			ta = tds.getAcct(params[0]);
			tds.close();

			// Set the non-database properties
			try {
				// Authenticate with persistent token stored in the database
        		AccessToken at = new AccessToken(ta.getToken(), ta.getTokenSecret());
        		twitter.setOAuthAccessToken(at);

        		// Once authenticated, retrieve User object
        		twitterUser = twitter.showUser(at.getUserId());

				// Obtain twitter information details for this user
				ta.setFollowers(twitterUser.getFollowersCount());
				ta.setFollowing(twitterUser.getFriendsCount());
				ta.setTwitterUser(twitterUser);
			} catch (TwitterException e) {
				e.printStackTrace();
			}

			return ta;
		}

		@Override
		protected void onPostExecute(TwitterAccount ta) {

			// Pass outcome (TwitterAccount object) of the worker thread
			// back to the calling activity, where it will be leveraged to
			// populate UI elements and any other necessary processing.
			((TwitterDetailActivity) context).processWorkerThreadResult(ta);
		}
	}

	/**
	 * Retrieve all persistent Twitter accounts from the database and display to UI
	 */
	public static class DisplayAccts extends AsyncTask<Void, Void, List<TwitterAccount>> {

		private Twitter twitter;
		private Context context;
		private int layoutResourceId;
		private final WeakReference<ListView> lvReference;

		public DisplayAccts(Context context, ListView lvReference, int layoutResourceId) {
			super();
			this.context = context;
			this.layoutResourceId = layoutResourceId;
			this.lvReference = new WeakReference<ListView>(lvReference);

			// Obtain twitter factory instance, to pull user information
			twitter = TwitterUtil.getInstance().getTwitter();
		}

		@Override
		protected List<TwitterAccount> doInBackground(Void... params) {
			User twitterUser;
			TwitterAccount ta;
			List<TwitterAccount> al;

			// Retrieve TwitterAccounts from database
			TwitterDataSource tds = new TwitterDataSource(context);
			tds.open();
			al = tds.getAllAccts();
			tds.close();

			// Loop through the TwitterAccounts and set the non-database properties
			for (int i = 0; i < al.size(); i++) {
				ta = al.get(i);

				try {
					// Authenticate with persistent token
					// stored in the database for each account
            		AccessToken at = new AccessToken(ta.getToken(), ta.getTokenSecret());
            		twitter.setOAuthAccessToken(at);

            		// Once authenticated, retrieve User object
            		twitterUser = twitter.showUser(at.getUserId());

					// Obtain twitter information details for this user
					ta.setFollowers(twitterUser.getFollowersCount());
					ta.setFollowing(twitterUser.getFriendsCount());
					ta.setTwitterUser(twitterUser);

					al.set(i, ta);
				} catch (Exception e) {
					e.printStackTrace();
    			}
			}

			return al;
		}

		@Override
		protected void onPostExecute(List<TwitterAccount> alAccounts) {

	        if (lvReference != null) {
	            final ListView lv = lvReference.get();

	            // Ensure the object reference to the ListView is still relevant
	            if (lv != null) {

	            	// Ensure at least one account available for display
	            	if (alAccounts.size() > 0) {
		            	DisplayAccountsAdapter daa = new DisplayAccountsAdapter(context, layoutResourceId, alAccounts);

		            	lv.setAdapter(daa);
		            	lv.setOnItemClickListener((OnItemClickListener) context);
	            	} else {
	            		lv.setVisibility(View.GONE);
	            	}
	            }
	        }
		}
	}

	/**
	 * Retrieve all followers of the specified Twitter account and store in the database
	 */
	public static class StoreFollowersIds extends AsyncTask<TwitterAccount, Void, Void> {

		private Context context;
		private Twitter twitter;

		public StoreFollowersIds(Context context) {
			super();
			this.context = context;

			// Obtain twitter factory instance, to pull user information
			twitter = TwitterUtil.getInstance().getTwitter();
		}

		@Override
		protected Void doInBackground(TwitterAccount... params) {
			TwitterAccount ta = params[0];

			// Authenticate with persistent token stored in the database
    		AccessToken at = new AccessToken(ta.getToken(), ta.getTokenSecret());
    		twitter.setOAuthAccessToken(at);

			try {
				IDs ids;
				int indexCounter = 1;
				int numberOfFollowers = ta.getFollowers();

				long cursor = -1;
				long[] local = new long[5000];
				long[] fArray = new long[numberOfFollowers];

//				int totalPages = 1 + numberOfFollowers / 5000;

				do {
					// The number of IDs returned is not guaranteed to be 5K per
					// page, as suspended users are filtered out after connections
					// are queried.
					ids = twitter.getFollowersIDs(cursor);
					local = ids.getIDs();

					System.arraycopy(local, 0, fArray, indexCounter - 1 , local.length);

				    indexCounter += local.length;

				    cursor = ids.getNextCursor();
				} while (ids.hasNext());

				// Insert followers into database
				TwitterDataSource tds = new TwitterDataSource(context);
				tds.open();
				tds.insertFollowers(ta.getTid(), fArray);
				tds.close();
			} catch(TwitterException e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	/**
	 * Delete all traces of the specified Twitter account from persistent storage
	 * in the database
	 */
	public static class LogOut extends AsyncTask<Long, Void, Void> {

		private Context context;

		public LogOut(Context context) {
			super();
			this.context = context;
		}

		@Override
		protected Void doInBackground(Long... params) {

			// Delete everything from the database associated with the specified Twitter account
			TwitterDataSource tds = new TwitterDataSource(context);
			tds.open();
			tds.deleteAcct(params[0]);
			tds.close();

			logoutFromTwitter();

			return null;
		}
	}

	/**
	 * Wrapper for {@link com.meromero.wlmp.twitter.TwitterUtil#reset() TwitterUtil.reset()}
	 */
	public static void logoutFromTwitter() {
		TwitterUtil.getInstance().reset();

		TwitterValues.TWITTER_IS_LOGGED_IN = false;
	}

}
package com.meromero.wlmp.twitter;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

/**
 * Helper class to handle retrieval of images from the Internet
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class ImageHelper {

	/**
	 *	Create a Bitmap object from an online image and assign it as
	 *	the source of an ImageView
	 */
	public static class CreateBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

		private final WeakReference<ImageView> ivReference;

		public CreateBitmapFromUrl(ImageView iv) {
			ivReference = new WeakReference<ImageView>(iv);
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap bitmap = null;

			try {
				URL url = new URL(params[0]);
				bitmap = BitmapFactory.decodeStream(url.openStream());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
	        if (ivReference != null && bitmap != null) {
	            final ImageView imageView = ivReference.get();

	            if (imageView != null) {
	                imageView.setImageBitmap(bitmap);
	            }
	        }
		}
	}

	/**
	 *	Create a Drawable object from an online image and assign it as
	 *	the source of an ImageView
	 */
	public static class CreateDrawableFromUrl extends AsyncTask<String, Void, Drawable> {

		private String srcName;
		private final WeakReference<ImageView> ivReference;

		public CreateDrawableFromUrl(ImageView ivReference, String srcName) {
			this.srcName = srcName;
			this.ivReference = new WeakReference<ImageView>(ivReference);
		}

		@Override
		protected Drawable doInBackground(String... params) {
			Drawable drawable = null;

			try {
				URL url = new URL(params[0]);
				drawable = Drawable.createFromStream(url.openStream(), srcName);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return drawable;
		}

		@Override
		protected void onPostExecute(Drawable drawable) {
	        if (ivReference != null && drawable != null) {
	            final ImageView imageView = ivReference.get();

	            if (imageView != null) {
	                imageView.setImageDrawable(drawable);
	            }
	        }
		}
	}

}
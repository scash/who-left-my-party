package com.meromero.wlmp.model;

/**
 * Class to bind data associated with a Twitter unfollower
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class TwitterUnfollower {

	private long fid;
	private long createdOn;

	public TwitterUnfollower() {		
	}

	public TwitterUnfollower(long fid, long createdOn) {
		this.fid = fid;
		this.createdOn = createdOn;
	}

	public long getFid() {
		return fid;
	}

	public void setFid(long fid) {
		this.fid = fid;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

}
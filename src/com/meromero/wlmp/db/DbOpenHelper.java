package com.meromero.wlmp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Helper class that handles database creation and modifications
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class DbOpenHelper extends SQLiteOpenHelper {

	private static final String LOGTAG = DbOpenHelper.class.getName();

	public DbOpenHelper(Context context) {
		super(context, DbContract.DATABASE_NAME, null, DbContract.DATABASE_VERSION);

		Log.i(LOGTAG, "DbOpenHelper constructor call");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Called if the DB does not exist

		db.execSQL(DbContract.TwitterAccount.CREATE_TABLE);
		db.execSQL(DbContract.TwitterFollower.CREATE_TABLE);
		db.execSQL(DbContract.TwitterUnfollower.CREATE_TABLE);

		Log.i(LOGTAG, "Tables have been created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Called if the Db already exist and it needs to be updated,
		//	think "ALTER TABLE" and such...managed via DATABASE_VERSION constant

		//
		// TODO: Once application gets published to Google Play, these should be
		// "ALTER TABLE" statements rather than table drops
		//
		db.execSQL("DROP TABLE IF EXISTS " + DbContract.TwitterAccount.DELETE_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + DbContract.TwitterFollower.DELETE_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + DbContract.TwitterUnfollower.DELETE_TABLE);

		onCreate(db);

		Log.i(LOGTAG, "DbOpenHelper.onUpgrade called");
	}

}
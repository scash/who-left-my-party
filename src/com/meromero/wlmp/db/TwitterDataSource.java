package com.meromero.wlmp.db;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import twitter4j.auth.AccessToken;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.meromero.wlmp.model.TwitterAccount;
import com.meromero.wlmp.model.TwitterUnfollower;

/**
 * Process all database requests related to Twitter data
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public class TwitterDataSource {

	private static final String LOGTAG = TwitterDataSource.class.getName();

	private SQLiteDatabase database;
	private SQLiteOpenHelper dbHelper;

	public TwitterDataSource(Context context) {
		dbHelper = new DbOpenHelper(context);
	}

	public void open() {
		database = dbHelper.getWritableDatabase();

		Log.i(LOGTAG, "Database opened");
	}

	public void close() {
		dbHelper.close();

		Log.i(LOGTAG, "Database closed");
	}

	/**
	 * Retrieve all Twitter accounts stored in the database
	 *
	 * @return	all {@link TwitterAccount} objects stored in the database
	 */
	public List<TwitterAccount> getAllAccts() {
		List<TwitterAccount> accts = new ArrayList<TwitterAccount>();

        // Grab all Twitter accounts
        String sqlQuery = "SELECT *" +
        		" FROM " + DbContract.TwitterAccount.TABLE_NAME +
        		" ORDER BY " + DbContract.TwitterAccount.COLUMN_HANDLE;

		Cursor c = database.rawQuery(sqlQuery, null);

		if (c != null) {
			if (c.moveToFirst()) {
				boolean active;
				TwitterAccount ta;

				do {
					ta = new TwitterAccount();

					ta.setTid(c.getLong(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_TID)));
					ta.setToken(c.getString(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_TOKEN)));
					ta.setTokenSecret(c.getString(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_TOKEN_SECRET)));
					ta.setHandle(c.getString(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_HANDLE)));

					active = (c.getShort(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_ACTIVE)) == 1) ? true : false;
					ta.setActive(active);

					accts.add(ta);
				} while (c.moveToNext());
			}

			c.close();
		}

		return accts;
	}

	/**
	 * Retrieve stored data for the indicated Twitter account
	 *
	 * @param	tid	unique identifier of the Twitter account
	 * @return	a {@link TwitterAccount} object representing the specified Twitter account
	 */
	public TwitterAccount getAcct(long tid) {
		TwitterAccount ta = null;

		// Prepare id parameter for WHERE clause
		String[] id = getIdArray(tid);

        // Query to grab Twitter account data
        String sqlQuery = "SELECT *" +
        		" FROM " + DbContract.TwitterAccount.TABLE_NAME +
        		" WHERE " + DbContract.TwitterAccount.COLUMN_TID + " = ?";

        Cursor c = database.rawQuery(sqlQuery, id);

		if (c != null) {
			if (c.moveToFirst()) {
				ta = new TwitterAccount();

				ta.setTid(c.getLong(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_TID)));
				ta.setToken(c.getString(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_TOKEN)));
				ta.setTokenSecret(c.getString(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_TOKEN_SECRET)));
				ta.setHandle(c.getString(c.getColumnIndex(DbContract.TwitterAccount.COLUMN_HANDLE)));
				ta.setActive(true);
			}

			c.close();
		}

		return ta;
	}

	/**
	 * Insert a new Twitter account into the database
	 *
	 * @param	at	the {@link AccessToken} containing authentication information
	 */
	public void insertAcct(AccessToken at) {

		// Prepare id parameter for delete WHERE clause
		String[] id = getIdArray(at.getUserId());

		// Start transaction
		database.beginTransaction();

		try {
			ContentValues cv = new ContentValues();

			// Delete any previous instance of the twitter account
			database.delete(DbContract.TwitterAccount.TABLE_NAME, DbContract.TwitterAccount.COLUMN_TID + " = ?", id);

			cv.put(DbContract.TwitterAccount.COLUMN_TID, at.getUserId());
			cv.put(DbContract.TwitterAccount.COLUMN_TOKEN, at.getToken());
			cv.put(DbContract.TwitterAccount.COLUMN_TOKEN_SECRET, at.getTokenSecret());
			cv.put(DbContract.TwitterAccount.COLUMN_HANDLE, at.getScreenName());

			database.insert(DbContract.TwitterAccount.TABLE_NAME, null, cv);

			database.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalStateException ise) {
			ise.printStackTrace();
		} finally {
			database.endTransaction();
		}
	}

	/**
	 * Obtain the number of followers stored in the database for
	 * the specified Twitter account
	 *
	 * @param	tid	unique identifier of the Twitter account to retrieve followers for
	 * @return	the number of followers for the specified Twitter account
	 */
	public int getTotalFollowers(long tid) {
		int totalFollowers;

		// Prepare id parameter for WHERE clause
		String[] id = getIdArray(tid);

		long result = DatabaseUtils.queryNumEntries(database,
				DbContract.TwitterFollower.TABLE_NAME,
				DbContract.TwitterFollower.COLUMN_TID + " = ?", id);

		totalFollowers = (int) result;

		return totalFollowers;
	}

	/**
	 * Retrieve all followers for a specified Twitter account
	 *
	 * @param	tid	unique identifier of the Twitter account to retrieve followers for
	 * @return	the identifiers of all followers
	 */
	public Set<Long> getFollowerIds(long tid) {
		Set<Long> fIds = new HashSet<Long>();

		// Prepare id parameter for WHERE clause
		String[] id = getIdArray(tid);

        // Grab all followers for a given TwitterAccount
        String sqlQuery = "SELECT " + DbContract.TwitterFollower.COLUMN_FID +
        		" FROM " + DbContract.TwitterFollower.TABLE_NAME +
        		" WHERE " + DbContract.TwitterFollower.COLUMN_TID + " = ?";

		Cursor c = database.rawQuery(sqlQuery, id);

		if (c != null) {
			if (c.moveToFirst()) {
				do {
					fIds.add(Long.valueOf(c.getLong(c.getColumnIndex(DbContract.TwitterFollower.COLUMN_FID))));
				} while (c.moveToNext());
			}

			c.close();
		}

		return fIds;
	}

	/**
	 * Store all followers for a specified Twitter account in the database
	 *
	 * @param	tid	unique identifier of the Twitter account
	 * @param	followers	the identifier of all followers to store in database
	 */
	public void insertFollowers(long tid, long[] followers) {

		// Prepare id parameter for WHERE clause
		String[] id = getIdArray(tid);

		// INSERT statement query
		String sqlQuery = "INSERT INTO " +
				DbContract.TwitterFollower.TABLE_NAME + " (" +
				DbContract.TwitterFollower.COLUMN_TID + "," +
				DbContract.TwitterFollower.COLUMN_FID + ") VALUES (?,?)";

		// Start transaction
		database.beginTransaction();

		try {
			// Delete any previous followers for this user
			database.delete(DbContract.TwitterFollower.TABLE_NAME, DbContract.TwitterFollower.COLUMN_TID + " = ?", id);

			SQLiteStatement biStatement = database.compileStatement(sqlQuery);

			for (int i = 0; i < followers.length; i++) {
				biStatement.clearBindings();

				biStatement.bindLong(1, tid);
			    biStatement.bindLong(2, followers[i]);
			    biStatement.executeInsert();
			}

			// Table cleanup
			database.delete(DbContract.TwitterFollower.TABLE_NAME, DbContract.TwitterFollower.COLUMN_TID + " = 0", null);

			database.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalStateException ise) {
			ise.printStackTrace();
		} finally {
			database.endTransaction();
		}
	}

	// This isn't an elegant bulk INSERT approach, but occasionally
	// on large data sets, it executes faster. Perform additional tests
	// to determine viability.
//	public void insertFollowersAlternate(long tid, long[] followers) {
//
//		//
//		// NOTE: sqlite will only allow 500 insertions via a SELECT UNION
//		//	and hence must break the bulk insert into blocks of 500
//		//
//
//		StringBuilder sqlInsert;
//		ArrayList<String> insertStatements = new ArrayList<String>();
//
//		int loopIteration;
//		int innerCounter = 0;
//		int outerCounter = (followers.length / 500) + 1;
//
//		// Construct bulk INSERT statement(s)
//		for (int i = 1; i <= outerCounter; i++) {
//			sqlInsert = new StringBuilder();
//
//			sqlInsert.append("INSERT INTO " + DbContract.TwitterFollower.TABLE_NAME + " ");
//
//			loopIteration = (followers.length > (i * 500)) ? (i * 500) : followers.length;
//
//			while (innerCounter < loopIteration) {
//				sqlInsert.append("SELECT " + tid + " as tid, " + followers[innerCounter] + " as fid ");
//				sqlInsert.append("UNION ");
//
//				innerCounter++;
//			}
//
//			// Extract extra "UNION" from the insert statement
//			sqlInsert.delete(sqlInsert.lastIndexOf("UNION "), sqlInsert.length());
//
//			insertStatements.add(sqlInsert.toString());
//		}
//System.out.println("tally: " + innerCounter);
//		// Prepare id parameter for WHERE clause
//		String[] id = getIdArray(tid);
//
//long startTime = System.currentTimeMillis();
//
//		// Start transaction
//		database.beginTransaction();
//
//		try {
//			// Delete any previous followers for this user
//			database.delete(DbContract.TwitterFollower.TABLE_NAME, DbContract.TwitterFollower.COLUMN_TID + " = ?", id);
//
//			// Insert current followers
//			for (String sql : insertStatements) {
//
//				// This is nasty ... figure out a better way!
//				Cursor c = database.rawQuery(sql, null);
//				c.close();
//			}
//
//			// Table cleanup
//			database.delete(DbContract.TwitterFollower.TABLE_NAME, DbContract.TwitterFollower.COLUMN_TID + " = 0", null);
//
//			database.setTransactionSuccessful();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			database.endTransaction();
//		}
//
//long diff = System.currentTimeMillis() - startTime;
//System.out.println(Long.toString(diff) + "ms to insert " + innerCounter + " records");	
//	}

	/**
	 * Retrieve all unfollowers for a specified Twitter account
	 *
	 * @param	tid	unique identifier of the Twitter account to retrieve unfollowers for
	 * @return	all {@link TwitterUnfollower} objects associated with the specified Twitter account
	 */
	public List<TwitterUnfollower> getUnfollowerIds(long tid) {
		List<TwitterUnfollower> unfollowerIds = new ArrayList<TwitterUnfollower>();

		// Prepare id parameter for WHERE clause
		String[] id = getIdArray(tid);

        // Grab all unfollowers for a given TwitterAccount
        String sqlQuery = "SELECT " +
        		DbContract.TwitterUnfollower.COLUMN_FID + ", " +
        		DbContract.TwitterUnfollower.COLUMN_DATE +
        		" FROM " + DbContract.TwitterUnfollower.TABLE_NAME +
        		" WHERE " + DbContract.TwitterUnfollower.COLUMN_TID + " = ?" +
        		" ORDER BY " + DbContract.TwitterUnfollower.COLUMN_DATE;

		Cursor c = database.rawQuery(sqlQuery, id);

		if (c != null) {
			if (c.moveToFirst()) {
				TwitterUnfollower tu;

				do {
					tu = new TwitterUnfollower();

					tu.setFid(c.getLong(c.getColumnIndex(DbContract.TwitterUnfollower.COLUMN_FID)));
					tu.setCreatedOn(c.getLong(c.getColumnIndex(DbContract.TwitterUnfollower.COLUMN_DATE)));

					unfollowerIds.add(tu);
				} while (c.moveToNext());
			}

			c.close();
		}

		return unfollowerIds;
	}

	/**
	 * Delete all traces of the specified Twitter account from the database
	 *
	 * @param	tid	unique identifier of the Twitter account
	 */
	public void deleteAcct(long tid) {

		// Prepare id parameter for delete WHERE clause
		String[] id = getIdArray(tid);

		// Start transaction
		database.beginTransaction();

		try {
			//
			// TODO: Figure out if SQLite supports cascading deletes
			//

			// Delete instance of account from primary table
			database.delete(DbContract.TwitterAccount.TABLE_NAME, DbContract.TwitterAccount.COLUMN_TID + " = ?", id);

			// Delete any stored followers
			database.delete(DbContract.TwitterFollower.TABLE_NAME, DbContract.TwitterFollower.COLUMN_TID + " = ?", id);

			// Delete any stored unfollowers
			database.delete(DbContract.TwitterUnfollower.TABLE_NAME, DbContract.TwitterUnfollower.COLUMN_TID + " = ?", id);

			database.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalStateException ise) {
			ise.printStackTrace();
		} finally {
			database.endTransaction();
		}
	}

	/**
	 * A convenience method to prepare an identifier for usage in a SQL query
	 *
	 * @param	id	identifier
	 * @return	a SQL-safe string representation of the identifier
	 */
	private String[] getIdArray(long id) {
		String[] idArray = new String[1];
		idArray[0] = Long.toString(id);

		return idArray;
	}

}
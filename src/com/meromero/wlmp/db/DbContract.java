package com.meromero.wlmp.db;

import android.provider.BaseColumns;

/**
 * Database contract for the entire application
 *
 * @author Santos R. Cash
 * @since 1.0
 */
public final class DbContract {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "wlmp.db";

	public DbContract() {}

    public static abstract class TwitterAccount implements BaseColumns {
        public static final String TABLE_NAME = "twitter_account";
        public static final String COLUMN_TID = "tid";
        public static final String COLUMN_TOKEN = "token";
        public static final String COLUMN_TOKEN_SECRET = "token_secret";
        public static final String COLUMN_HANDLE = "handle";
        public static final String COLUMN_ACTIVE = "active";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                COLUMN_TID + " INTEGER PRIMARY KEY, " +
                COLUMN_TOKEN + " TEXT, " +
                COLUMN_TOKEN_SECRET + " TEXT, " +
                COLUMN_HANDLE + " TEXT, " +
                COLUMN_ACTIVE + " INTEGER DEFAULT 0)";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class TwitterFollower implements BaseColumns {
        public static final String TABLE_NAME = "twitter_follower";
        public static final String COLUMN_TID = "tid";
        public static final String COLUMN_FID = "fid";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                COLUMN_TID + " INTEGER, " +
                COLUMN_FID + " INTEGER)";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class TwitterUnfollower implements BaseColumns {
        public static final String TABLE_NAME = "twitter_unfollower";
        public static final String COLUMN_TID = "tid";
        public static final String COLUMN_FID = "fid";
        public static final String COLUMN_DATE = "created_on";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                COLUMN_TID + " INTEGER, " +
                COLUMN_FID + " INTEGER, " +
                COLUMN_DATE + " INTEGER DEFAULT (strftime('%s', 'now')))";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

}